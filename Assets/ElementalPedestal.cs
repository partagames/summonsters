﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class ElementalPedestal : MonoBehaviour {

    public enum ElementalPedestalState
    {
        SPAWNING,
        SPAWNED
    }

    public GameObject[] elementalPrefabs;
    private Transform spawnPoint;

    public float spawnTime = 2f;
    public GameObject currentElementalPrefab;
    public ElementalPedestalState state;
    public float spawnTimer;

    private GameObject spawnedElemental;

    // Use this for initialization
    void Start () {
        spawnPoint = transform.Find("ElementalSpawn");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	    if (state == ElementalPedestalState.SPAWNING)
        {
            spawnTimer += Time.deltaTime;
            if (spawnTimer >= spawnTime)
            {
                Spawned();
            }
        } else if (state == ElementalPedestalState.SPAWNED)
        {
            if (spawnedElemental == null)
            {
                Spawn();
            }
        }
	}

    private void Spawned()
    {
        if (state != ElementalPedestalState.SPAWNED)
        {
            state = ElementalPedestalState.SPAWNED;

            if (currentElementalPrefab == null)
            {
                currentElementalPrefab = Utils.GetRandom(elementalPrefabs);
            }

            spawnedElemental = Instantiate<GameObject>(currentElementalPrefab);
            spawnedElemental.transform.position = spawnPoint.transform.position;
            spawnPoint.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            spawnPoint.transform.rotation = Quaternion.identity;

            spawnPoint.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f,0f);
        }
    }

    void Spawn()
    {
        state = ElementalPedestalState.SPAWNING;

        currentElementalPrefab = Utils.GetRandom(elementalPrefabs);
        spawnTimer = 0;
        spawnPoint.GetComponent<SpriteRenderer>().sprite = currentElementalPrefab.GetComponentInChildren<SpriteRenderer>().sprite;
        spawnPoint.GetComponent<SpriteRenderer>().DOColor(new Color(1f, 1f, 1f, 1f), 0.5f);


        spawnPoint.transform.DOScale(1, spawnTime);
        spawnPoint.transform.DORotate(new Vector3(0, 0, spawnTime * 360f), spawnTime, RotateMode.FastBeyond360);

    }
}
