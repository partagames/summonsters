﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	public float lerpSpeed;
	private Transform hero;
    private GameRules gameRules;

	// Use this for initialization
	void Start () {
		hero = GameObject.Find ("Hero").transform;
        gameRules = FindObjectOfType<GameRules>();
	}
	
	// Update is called once per frame
	void Update () {
        if (gameRules.state == GameState.PLAY)
        {
            var newPos = Vector3.Lerp(transform.position, hero.position, lerpSpeed * Time.deltaTime);
            transform.position = new Vector3(newPos.x, newPos.y, transform.position.z);
        }
    }
}
