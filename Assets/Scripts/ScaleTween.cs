﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ScaleTween : MonoBehaviour {

	public Vector3 targetScale = new Vector3(0f, 0f ,0f);
	public float delaySeconds = 0f;
	public float duration;
	public LoopType loopType;
	public int loops;
	public Ease easeType;

	private Vector3 originalScale;

	// Use this for initialization
	void OnEnable() {
		originalScale = transform.localScale;
		StartCoroutine(Tween());
	}

	IEnumerator Tween()
	{
		yield return new WaitForSeconds(delaySeconds);
		transform.DOScale (targetScale, duration).SetEase (easeType).SetLoops (loops, loopType);
		yield return null;
	}
}