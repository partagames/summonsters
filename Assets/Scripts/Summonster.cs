﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public class Summonster : MonoBehaviour
{

    public float lerpSpeed;
    public SummonsterState state = SummonsterState.FOLLOW;
    public Transform followTarget;
    public Transform attackTarget;
    private float searchRadius;
    private Vector3 finalPos = new Vector3();
    private Hero hero;

	public ElementType bodyElement;
	public ElementType handElement;
	public ElementType feetElement;
    
    // STATS
    public float attackSpeed;
	public int damage;
	public int health;

    private float attackTimer;

	public GameObject scrollingTextPrefab;
	private Canvas textCanvas;

	public GameObject hands;
	public GameObject feet;
    private GameRules gameRules;
    public AudioClip swingClip;
    public AudioClip dispelClip;

    private AudioSource audioSource;

    public enum SummonsterState
    {
        FOLLOW,
        ATTACK,
        DEAD
    }

    // Use this for initialization
    void Start()
    {
        hero = FindObjectOfType<Hero>();
		textCanvas = transform.GetComponentInChildren<Canvas> ();

        followTarget = hero.transform;

		MonsterAttributes calculatedBaseValues = DamageLogic.CalculateBaseStats (transform.GetComponentsInChildren<MonsterAttributes> ());

        searchRadius = 4f; // TODO: FROM MODULES?!?!?!
		attackSpeed = calculatedBaseValues.baseAttackSpeed;
		damage = calculatedBaseValues.baseDamage;
		health = calculatedBaseValues.baseHealth;
        gameRules = FindObjectOfType<GameRules>();
        audioSource = gameObject.AddComponent<AudioSource>();
    }
    
    void FixedUpdate()
    {
        if (gameRules.state != GameState.PLAY)
        {
            return;
        }

        if (health <= 0 && state != SummonsterState.DEAD)
        {
            Die();
        }

		feet.transform.Rotate (new Vector3 (0f, 0f, 5f));

		if (state == SummonsterState.FOLLOW)
        {
            UpdateFollow();
        }
        else if (state == SummonsterState.ATTACK)
        {
            UpdateAttack();
        }
    }

    private void UpdateFollow()
    {
        if (Vector2.Distance(transform.position, followTarget.position) > 1.5f)
        {
            var direction = transform.position - followTarget.position;
            direction.Normalize();
            finalPos = followTarget.position + direction * 1.5f;

            var force = finalPos - transform.position;
            force.Normalize();

            GetComponent<Rigidbody2D>().AddForce(force * 350);

            //var newPos = Vector3.Lerp(transform.position, finalPos, lerpSpeed * Time.deltaTime);
            //transform.position = new Vector3(newPos.x, newPos.y, transform.position.z);
        }
    }

    private void Die()
    {
		Debug.Log ("SUMMONSTER DIE! state= " + state);

		state = SummonsterState.DEAD;

		int randomDrop = UnityEngine.Random.Range (0, 2);
		ElementType dropElement = ElementType.UNKNOWN;
		if (randomDrop == 0) {
			dropElement = GetAttackElement ();
		} else if (randomDrop == 1) {
			dropElement = GetBodyElement ();
		}

		if (dropElement != ElementType.UNKNOWN) {
			// re-drop some of the materials
			switch (dropElement) {
			case ElementType.AIR:
				GameObject.Instantiate (Resources.Load ("Collectibles/BasicAirMote"), transform.position, transform.rotation);
				break;
			case ElementType.EARTH:
				GameObject.Instantiate (Resources.Load ("Collectibles/BasicEarthMote"), transform.position, transform.rotation);
				break;
			case ElementType.FIRE:
				GameObject.Instantiate (Resources.Load ("Collectibles/BasicFireMote"), transform.position, transform.rotation);
				break;
			case ElementType.ICE:
				GameObject.Instantiate (Resources.Load ("Collectibles/BasicWaterMote"), transform.position, transform.rotation);
				break;
			}
		}

		foreach (SpriteRenderer sprite in GetComponentsInChildren<SpriteRenderer> ()) {
			sprite.enabled = false;
		}

		var particle = transform.GetComponentInChildren<ParticleSystem> ();
		particle.Simulate (particle.duration);

		StartCoroutine (DestroySummonster (0.8f));

        audioSource.PlayOneShot(dispelClip);
    }

	public IEnumerator DestroySummonster(float delay) {
		yield return new WaitForSeconds (delay);
		Destroy (gameObject);
		yield return null;
	}

    private void UpdateAttack()
    {
        if (attackTarget == null)
        {
            var colliders = Physics2D.OverlapCircleAll(transform.position, searchRadius);
            foreach (Collider2D collider in colliders)
            {
                if (collider.gameObject.layer == LayerMask.NameToLayer("Enemy"))
                {
                    attackTarget = collider.transform;
                    break;
                }
            }
        }

        if (attackTarget != null)
        {

            if (Vector2.Distance(transform.position, attackTarget.position) > 2f)
            {
                // MOVE CLOSER!


                finalPos = attackTarget.position;

                var force = finalPos - transform.position;
                force.Normalize();

                GetComponent<Rigidbody2D>().AddForce(force * 350);
                /*
                var newPos = Vector3.Lerp(transform.position, attackTarget.position, lerpSpeed * Time.deltaTime);
                transform.position = new Vector3(newPos.x, newPos.y, transform.position.z);
                */
            }
            else
            {
                // ACTUALLY SWING
                if (attackTimer <= 0)
                {
                    audioSource.PlayOneShot(swingClip);
					hands.transform.DOLocalMoveX(0.2f, 0.2f)
						.SetLoops(2, LoopType.Yoyo)
						.SetEase(Ease.InOutBack);

                    Enemy enemy = attackTarget.gameObject.GetComponent<Enemy>();
                    if (enemy != null)
                    {
                        enemy.InflictDamage(this, damage); // TODO: TYPE; POWER; ETC
                    }
                    attackTimer = attackSpeed;
                }
                else
                {
                    attackTimer -= Time.deltaTime;
                }

            }
        }
        else
        {
            var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //var direction = transform.position - mousePos;
            //direction.Normalize();
            //finalPos = followTarget.position + direction * 1.5f;
            finalPos = mousePos;

            var force = finalPos - transform.position;
            force.Normalize();

            GetComponent<Rigidbody2D>().AddForce(force * 350);

            //var newPos = Vector3.Lerp(transform.position, finalPos, lerpSpeed * Time.deltaTime);
            //transform.position = new Vector3(newPos.x, newPos.y, transform.position.z);
        }
    }

    public void Attack()
    {
		if (state != SummonsterState.DEAD)
        	state = SummonsterState.ATTACK;
    }

    public void Follow()
    {
        if (state != SummonsterState.DEAD)
        {
            state = SummonsterState.FOLLOW;
            attackTarget = null;
        }
    }

    public void OnDrawGizmos()
    {
        Gizmos.DrawCube(finalPos, new Vector3(0.1f,0.1f,0.1f));
    }

	public void InflictDamage(ElementType attackElement, int damage) {
		int calculatedDamage = DamageLogic.calculateInflictedDamage(damage, attackElement, GetBodyElement());
		health -= calculatedDamage;

		var text = Instantiate<GameObject>(scrollingTextPrefab);
		text.GetComponent<ScrollingText>().Setup("-" + calculatedDamage);
		text.transform.SetParent(textCanvas.transform, false);
	}

    public void InflictDamage(Enemy hitter, int damage)
    {
		int calculatedDamage = DamageLogic.calculateInflictedDamage(damage, hitter.GetAttackElement(), GetBodyElement());
		health -= calculatedDamage;
        
		var text = Instantiate<GameObject>(scrollingTextPrefab);
		text.GetComponent<ScrollingText>().Setup("-" + calculatedDamage);
		text.transform.SetParent(textCanvas.transform, false);
    }

	public ElementType GetFeetElement() {
		return feetElement;
	}

	public ElementType GetBodyElement() {
		return bodyElement;
	}

	public ElementType GetAttackElement() {
		return handElement;
	}

}
