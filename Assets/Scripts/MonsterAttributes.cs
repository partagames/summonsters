﻿using UnityEngine;
using System.Collections;

public class MonsterAttributes : MonoBehaviour {

	public int baseHealth;
	public int baseDamage;
	public float baseAttackSpeed;

}
