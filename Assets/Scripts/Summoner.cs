﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

[System.Serializable]
public enum ElementType
{
    UNKNOWN = -1,
    ICE = 0,
    FIRE,
    EARTH,
    AIR
}

public class Summoner : MonoBehaviour {
    public ElementType selectedElement = ElementType.UNKNOWN;
    public ElementType bodyElement = ElementType.UNKNOWN;
    public ElementType handElement = ElementType.UNKNOWN;
    public ElementType feetElement = ElementType.UNKNOWN;
    public ElementType eyeElement = ElementType.UNKNOWN;

    public GameObject summonMenu;

    public GameObject monsterBasePrefab;
    public GameObject inventoryItemPrefab;

    /// <summary>
    /// Contains all InventoryItems
    /// </summary>
    public RectTransform inventory;

    public Dictionary<ElementType, InventoryItem> inventoryItems;

    public Text invetoryText;

	private bool summoning = false;
    private AudioSource audioSource;

    // Use this for initialization
    void Start ()
    {
        summonMenu.SetActive(false);
        audioSource = GetComponent<AudioSource>();
        inventoryItems = new Dictionary<ElementType, InventoryItem>();
    }
	
	// Update is called once per frame
	void Update ()
    {
/*
        if (Input.GetKeyDown("x"))
        {
            summonMenu.SetActive(!summonMenu.activeSelf);
        }

		if (Input.GetKey ("h")) {
			GameObject.FindObjectOfType<GameRules> ().Win ();
		}
        if (Input.GetKeyDown("g"))
        {
            var values = Enum.GetValues(typeof(ElementType));
            bodyElement = (ElementType) values.GetValue(UnityEngine.Random.Range(0, values.Length));
            handElement = (ElementType) values.GetValue(UnityEngine.Random.Range(0, values.Length));
			feetElement = (ElementType) values.GetValue (UnityEngine.Random.Range (0, values.Length));
            Summon();
        }
        */
        UpdateInvetory();

		if (!summoning)
			if (handElement != ElementType.UNKNOWN && feetElement != ElementType.UNKNOWN && bodyElement != ElementType.UNKNOWN) {
				StartCoroutine(Summon());
			}
    }

    private void UpdateInvetory()
    {
        var hero = FindObjectOfType<Hero>();
        string inventoryString = "";
        foreach (ElementType type in hero.inventory.Keys)
        {
            inventoryString += type + " " + hero.inventory[type] + "\n";
            if (!inventoryItems.ContainsKey(type))
            {
                var obj = Instantiate<GameObject>(inventoryItemPrefab);
                obj.transform.SetParent(inventory, false);

                var inventoryItem = obj.GetComponent<InventoryItem>();
                inventoryItem.Setup(type, hero.inventory[type]);
                inventoryItems.Add(type, inventoryItem);
            }

            inventoryItems[type].SetCount(hero.inventory[type]);
        }
//        invetoryText.text = inventoryString;
    }

    public IEnumerator Summon()
    {
		summoning = true;
		yield return new WaitForSeconds (0.5f);

        FindObjectOfType<GameRules>().Play();

        audioSource.Play();

        var hero = FindObjectOfType<Hero>();
        if (hero != null)
        {
			hero.Summoning ();
			StartCoroutine (SpawnMonster (hero, 1));
        }
		yield return null;
    }

	public IEnumerator SpawnMonster(Hero hero, int delay) {
		yield return new WaitForSeconds (delay);

		GameObject monster = Instantiate<GameObject>(monsterBasePrefab);

		var m = monster.GetComponent<Summonster> ();
		m.bodyElement = bodyElement;
		m.handElement = handElement;
		m.feetElement = feetElement;

		if (bodyElement != ElementType.UNKNOWN)
		{
			GameObject prefab = Resources.Load<GameObject>(bodyElement + "_BODY");
			var obj = Instantiate<GameObject>(prefab);
			obj.transform.parent = monster.transform;
        }
		if (handElement != ElementType.UNKNOWN)
		{
			GameObject prefab = Resources.Load<GameObject>(handElement + "_HAND");
			var obj = Instantiate<GameObject>(prefab);
			obj.transform.parent = monster.transform;
			m.hands = obj;
        }
		if (feetElement != ElementType.UNKNOWN) {
			GameObject prefab = Resources.Load<GameObject>(feetElement + "_LEGS");
			var obj = Instantiate<GameObject>(prefab);
			obj.transform.parent = monster.transform;
			m.feet = obj;
		}

		{
			GameObject prefab = Resources.Load<GameObject> ("RANDOM_EYES");
			var obj = Instantiate<GameObject> (prefab);
			obj.transform.parent = monster.transform;
		}

        var spawnPos = hero.transform.position + new Vector3(-1.5f, 0f);
		monster.transform.position = spawnPos;

		hero.Normal ();

        handElement = ElementType.UNKNOWN;
        bodyElement = ElementType.UNKNOWN;
		feetElement = ElementType.UNKNOWN;

		summoning = false;

        yield return null;
	}

}

