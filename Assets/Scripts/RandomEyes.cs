﻿using UnityEngine;
using System.Collections;

public class RandomEyes : MonoBehaviour {

	public Sprite[] eyeSprites;

	// Use this for initialization
	void Start () {
		int randomEye = Random.Range (0, eyeSprites.Length);
		GetComponent<SpriteRenderer>().sprite = eyeSprites[randomEye];
	}
}
