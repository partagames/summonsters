﻿using UnityEngine;
using System.Collections;
using System;

public static class Utils {
    public static T GetRandom<T>(T[] array) {
        if (array.Length == 0) {
            return default(T);
        }
        return array[UnityEngine.Random.Range(0, array.Length)];
    }

    public static Vector2 LineIntersectionPoint(Vector2 ps1, Vector2 pe1, Vector2 ps2, Vector2 pe2) {
        // Get A,B,C of first line - points : ps1 to pe1
        float A1 = pe1.y - ps1.y;
        float B1 = ps1.x - pe1.x;
        float C1 = A1 * ps1.x + B1 * ps1.y;

        // Get A,B,C of second line - points : ps2 to pe2
        float A2 = pe2.y - ps2.y;
        float B2 = ps2.x - pe2.x;
        float C2 = A2 * ps2.x + B2 * ps2.y;

        // Get delta and check if the lines are parallel
        float delta = A1 * B2 - A2 * B1;
        if (delta == 0)
            return new Vector2(0, 0);

        // now return the Vector2 intersection point
        return new Vector2(
            (B2 * C1 - B1 * C2) / delta,
            (A1 * C2 - A2 * C1) / delta
        );
    }

    /// <summary>
    /// Returns a child with the given tag and null if one could not be found.
    /// </summary>
    /// <param name="gameObject"></param>
    /// <param name="tag"></param>
    /// <returns></returns>
    public static GameObject GetChildWithTag(GameObject gameObject, string tag) {
        foreach (Transform child in gameObject.transform) {
            if (child.CompareTag(tag)) {
                return child.gameObject;
            }
        }
        return null;
    }
    
}
