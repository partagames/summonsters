﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {
    private static MusicPlayer instance = null;

    private AudioSource audioSource;
    public AudioClip menuMusic;
    public AudioClip gameMusic;

    public static MusicPlayer Instance {
        get { return instance; }
    }

    void Awake() {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
            return;
        } else {
            instance = this;
        }
        DontDestroyOnLoad(this.gameObject);
        audioSource = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start () {
        StartMusic();
	}

	// Update is called once per frame
	void Update () {
	
	}
    
    public void StartMenuMusic() {
        if (!audioSource.clip.Equals(menuMusic)) {
            audioSource.Stop();
            audioSource.clip = menuMusic;
        }
        if (!audioSource.isPlaying) {
            audioSource.Play();
        }
   }

    public void StartMusic() {
        if (!audioSource.clip.Equals(gameMusic)) {
            audioSource.Stop();
            audioSource.clip = gameMusic;
        }
        if (!audioSource.isPlaying) {
            audioSource.Play();
        }
    }

    public void StopMusic() {
        audioSource.Stop();
    }

}
