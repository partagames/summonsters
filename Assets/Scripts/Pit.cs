﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pit : MonoBehaviour
{

    public float damageInterval;
    public int damage;
    public ElementType weaknessType;
    public ElementType damageType;

    private List<Collider2D> victims = new List<Collider2D>();

    private float stateTime = 0f;

    void FixedUpdate()
    {
        stateTime += Time.deltaTime;

        if (stateTime > damageInterval)
        {
            for (int i = 0; i < victims.Count; i++) 
            {
                var victim = victims[i];
                if (victim == null)
                {
                    victims.RemoveAt(i);
                }
                else if (victim.transform.tag == "Summonster" && stateTime > damageInterval)
                {
                    var summonster = victim.transform.GetComponent<Summonster>();
                    if (summonster.feetElement == weaknessType)
                    {
                        victim.transform.GetComponent<Summonster>().InflictDamage(damageType, damage);
                    }
                }
            }
            stateTime = 0f;
        }
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        victims.Add(collision);
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        victims.Remove(collision);
    }

}
