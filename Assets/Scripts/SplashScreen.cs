﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour {

    public float splashDuration;

    private float stateTime = 0f;

    void Start()
    {
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
        Screen.orientation = ScreenOrientation.AutoRotation;
    }

    void Update()
    {
        stateTime += Time.deltaTime;
        if (stateTime > splashDuration)
        {
            StartCoroutine(EndSplashScreen());
        }
    }

    IEnumerator EndSplashScreen()
    {
        float fadeTime = transform.GetComponent<Fading>().BeginFade(1);
        yield return new WaitForSeconds(fadeTime * 2);
        Application.LoadLevel("mapscene");
    }
        
}
