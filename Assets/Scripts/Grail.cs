﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Grail : MonoBehaviour {

    public enum GrailState
    {
        UNACCESSIBLE,
        ACCESSIBLE
    }

    private GrailState state;

    public Enemy[] bosses;

    public void Start()
    {
        state = GrailState.UNACCESSIBLE;
        GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.25f);
        GetComponentInChildren<ParticleSystem>().Stop();
    }

    public void Update()
    {
        if (state == GrailState.UNACCESSIBLE)
        {
            bool bossesDone = true;
            for (int i = 0; i < bosses.Length; i++)
            {
                Enemy boss = bosses[i];
                if (boss.state != EnemyState.DEAD)
                {
                    bossesDone = false;
                    break;
                }
            }
            if (bossesDone)
            {
                //
                state = GrailState.ACCESSIBLE;
                GetComponentInChildren<ParticleSystem>().Play();
                GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 1f);
            }
        } 
        else if (state == GrailState.ACCESSIBLE)
        {

        }
    }

	public void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.transform.tag == "Player" && state == GrailState.ACCESSIBLE)
		{
			GameObject.FindObjectOfType<GameRules> ().Win();
            transform.DOScale(0f, 0.25f).OnComplete(Collect);
		}
	}

    public void Collect()
    {
        Destroy(gameObject);
    }
}
