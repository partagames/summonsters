﻿using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

public enum EnemyState
{
    IDLE, ATTACK, MOVE, DEAD
}

public class Enemy : MonoBehaviour {

    public EnemyState state;
    public ElementType bodyElement;
    public ElementType attackElement;

    public float searchRadius;

    private Hero hero;
    private Summonster targetSummonster;

    private Transform attackTarget;

    public int damage = 50;
    public int health = 100;
    public float speed;

    public float attackSpeed = 1f;
    public float attackTimer;

    public GameObject scrollingTextPrefab;
    private Canvas textCanvas;

    private Animator anim;
    private GameRules gameRules;

    private AudioSource audioSource;
    public AudioClip swingClip;
    public AudioClip dieClip;
    public AudioClip attackClip;

	public float attackRadius = 1.5f;

    // Use this for initialization
    void Start()
    {
        hero = FindObjectOfType<Hero>();
        textCanvas = transform.GetComponentInChildren<Canvas>();
        anim = transform.GetComponentInChildren<Animator>();
        Idle();

        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.volume = 0.5f;

        gameRules = FindObjectOfType<GameRules>();
    }
	
	void FixedUpdate () {
        if (gameRules.state != GameState.PLAY)
        {
            return;
        }

		if (health <= 0 && state != EnemyState.DEAD)
        {
            Die();
        }

        if (state == EnemyState.DEAD)
        {
            return;
        }

        if (targetSummonster != null)
        {
            attackTarget = targetSummonster.transform;
        } else if (hero.state != Hero.HeroState.DEAD)
        {
            attackTarget = hero.transform;
        } else
        {
            Idle();
            return;
        }

		if (Vector2.Distance(transform.position, attackTarget.position) < attackRadius)
        {
            Attack();
        }
        else if (Vector2.Distance(transform.position, attackTarget.position) < searchRadius)
        {
            Move();
        } else
        {
            Idle();
        }

        if (state == EnemyState.IDLE)
        {
            UpdateIdle();
        } else if (state == EnemyState.MOVE)
        {
            UpdateMove();
        } else if (state == EnemyState.ATTACK)
        {
            UpdateAttack();
        }
	}

    void UpdateIdle()
    {
    }

    void UpdateAttack()
    {
        if (Vector2.Distance(transform.position, attackTarget.position) < 2f)
        {
            if (attackTimer <= 0f)
            {
                if (targetSummonster != null)
                {
                    targetSummonster.InflictDamage(this, damage);
                    
                    var child = transform.FindChild("Sprite");
                    if (child != null)
                    {
                        child.transform.DOLocalMove(new Vector3(0f, 0.125f, 0f), 0.2f)
                            .SetEase(Ease.InCubic)
                            .SetLoops(2, LoopType.Yoyo)
                            .OnComplete(DoMove);
                    }
                    audioSource.PlayOneShot(swingClip);

                    if (targetSummonster.state == Summonster.SummonsterState.DEAD)
                    {
                        targetSummonster = null;
                    }
                }
                else
                {
					hero.InflictDamage (this, damage);

                    var child = transform.FindChild("Sprite");
                    if (child != null)
                    {
                        child.transform.DOLocalMove(new Vector3(0.125f, 0.0f, 0f), 0.2f)
                            .SetEase(Ease.InCubic)
                            .SetLoops(2, LoopType.Yoyo)
                            .OnComplete(DoMove);
                    }
                    audioSource.PlayOneShot(swingClip);

                    if (hero.state == Hero.HeroState.DEAD)
                    {
                        Idle();
                    }
                }
                attackTimer = attackSpeed;
            }
            else
            {
                attackTimer -= Time.deltaTime;
            }
        }
    }

    void UpdateMove()
    {
        Vector2 direction = attackTarget.position - transform.position;
        direction.Normalize();

        GetComponent<Rigidbody2D>().AddForce(direction * speed);
    }

    void Idle()
    {
        state = EnemyState.IDLE;
    }
    void Attack()
    {
        if (state != EnemyState.ATTACK)
        {
            state = EnemyState.ATTACK;
        }
    }
    void Move()
    {
        if (state != EnemyState.MOVE)
        {
                print("moving ");
                state = EnemyState.MOVE;
            /*if (!attackPlayed)
            {

            }*/
            audioSource.PlayOneShot(attackClip);
        }
    }

    private void DoMove()
    {
    }

    private void Die()
    {
        state = EnemyState.DEAD;
		anim.SetTrigger ("dead");
		GetComponent<BoxCollider2D> ().enabled = false;

		DropElement (bodyElement);
		DropElement (attackElement);

        audioSource.PlayOneShot(dieClip);
    }

	public void DropElement(ElementType dropElement) {
		// drop some of the materials
		switch (dropElement) {
		case ElementType.AIR:
			GameObject.Instantiate(Resources.Load("Collectibles/BasicAirMote"), transform.position, transform.rotation);
			break;
		case ElementType.EARTH:
			GameObject.Instantiate(Resources.Load("Collectibles/BasicEarthMote"), transform.position, transform.rotation);
			break;
		case ElementType.FIRE:
			GameObject.Instantiate(Resources.Load("Collectibles/BasicFireMote"), transform.position, transform.rotation);
			break;
		case ElementType.ICE:
			GameObject.Instantiate(Resources.Load("Collectibles/BasicWaterMote"), transform.position, transform.rotation);
			break;
		}
	}

    internal void InflictDamage(Summonster hitter, int damage)
    {
        if (state != EnemyState.DEAD)
        {
			int calculatedDamage = DamageLogic.calculateInflictedDamage(damage, hitter.GetAttackElement(), bodyElement);
			health -= calculatedDamage;

			var text = Instantiate<GameObject>(scrollingTextPrefab);
			text.GetComponent<ScrollingText>().Setup("-" + calculatedDamage);
			text.transform.SetParent(textCanvas.transform, false);

            GetComponentInChildren<ParticleSystem>().Play();
            targetSummonster = hitter;
        }
    }

	public ElementType GetBodyElement() {
		return bodyElement;
	}

	public ElementType GetAttackElement() {
		return attackElement;
	}
}
