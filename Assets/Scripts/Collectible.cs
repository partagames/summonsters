﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

public enum CollectibleState
{
    ON_GROUND,
    COLLECTED,
    FINISHED
}

public class Collectible : MonoBehaviour
{
    private Hero collector;
    public CollectibleState state;
    public ElementType type;
    public AudioClip collectSound;
    private AudioSource audioSource;

    // Use this for initialization
    void Awake()
    {
        state = CollectibleState.ON_GROUND;
        audioSource = gameObject.AddComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (state == CollectibleState.FINISHED && !audioSource.isPlaying)
        {
            Destroy(gameObject);
        }

        if (state == CollectibleState.COLLECTED)
        {
			transform.GetComponentInChildren<PositionTween> ().KillTweens ();
			transform.position = Vector2.MoveTowards(transform.position, collector.transform.position, 0.1f);
            if (Vector2.Distance(transform.position, collector.transform.position) < 0.01f)
            {
				collector.Collect(this.type);
                GetComponentInChildren<SpriteRenderer>().enabled = false;
                state = CollectibleState.FINISHED;
                //Destroy(gameObject);
            }
        }
    }
    

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (state == CollectibleState.ON_GROUND && collision.transform.tag == "Player")
        {
            audioSource.PlayOneShot(collectSound);
            Collected(collision.gameObject.GetComponent<Hero>());
        }
    }
    
    private void Collected(Hero hero)
    {
        state = CollectibleState.COLLECTED;
        this.collector = hero;
    }
}
