﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Hero : MonoBehaviour {

    public float speed;
    public float edgeCollideDistance;
    public HeroState state = HeroState.NORMAL;
    public GameObject summonParticleSystemPrefab;

    private GameObject summonParticleInstance;
    private Animator anim;

    public int health = 100;

    // UI
    public GameObject scrollingTextPrefab;
    private Canvas textCanvas;

    private bool facingRight;

    public AudioClip[] hitSounds;

    private AudioSource audioSource;

	public enum HeroState {
		NORMAL,
		SUMMONING,
		ORDER_ATTACK,
		DEAD
	}

    public Dictionary<ElementType, int> inventory;
    private GameRules gameRules;

    // Use this for initialization
    void Start () {
		anim = GetComponent<Animator> ();
        inventory = new Dictionary<ElementType, int>();
        textCanvas = GetComponentInChildren<Canvas>();
        audioSource = gameObject.GetComponent<AudioSource>();
        gameRules = FindObjectOfType<GameRules>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (gameRules.state != GameState.PLAY)
        {
            return;
        }

		if (state == HeroState.ORDER_ATTACK) {
			anim.SetBool ("ordering", true);
		} else {
			anim.SetBool ("ordering", false);
		}

        if (state != HeroState.DEAD) {
			var pos = transform.position;

			if (Input.GetKey (KeyCode.UpArrow) || Input.GetKey (KeyCode.W)) {
				GetComponent<Rigidbody2D> ().AddForce (Vector2.up * speed);
			}
			if (Input.GetKey (KeyCode.DownArrow) || Input.GetKey (KeyCode.S)) {
				GetComponent<Rigidbody2D> ().AddForce (Vector2.down * speed);
			}
			if (Input.GetKey (KeyCode.LeftArrow) || Input.GetKey (KeyCode.A)) {
				GetComponent<Rigidbody2D> ().AddForce (Vector2.left * speed);
				Flip (false);
			}
			if (Input.GetKey (KeyCode.RightArrow) || Input.GetKey (KeyCode.D)) {
				GetComponent<Rigidbody2D> ().AddForce (Vector2.right * speed);
				Flip (true);
			}

			if (Input.GetKey (KeyCode.Z) || Input.GetMouseButton(0)) {
				OrderAttack ();
			} else if (state != HeroState.SUMMONING) {
				Normal ();
			}

			if (health <= 0) {
				Dead ();
			}

			if (summonParticleInstance != null)
				summonParticleInstance.transform.position = transform.position;
		}
    }

	public void Flip(bool facingRight) {
		var scale = transform.localScale;
		if (facingRight) {
			transform.localScale = new Vector3 (Mathf.Abs (scale.x), scale.y, scale.z);

			var canvasScale = textCanvas.transform.localScale;
			textCanvas.transform.localScale = new Vector3 (Mathf.Abs (canvasScale.x), canvasScale.y, canvasScale.z);
		} else if (scale.x > 0f) {
			transform.localScale = new Vector3 (-scale.x, scale.y, scale.z);

			var canvasScale = textCanvas.transform.localScale;
			textCanvas.transform.localScale = new Vector3 (-canvasScale.x, canvasScale.y, canvasScale.z);
		}
	}

	internal void Collect(ElementType type)
    {
		StartCoroutine (ShowCollectText (type));

        if (inventory.ContainsKey(type))
        {
            inventory[type]++;
        }
        else
        {
            inventory.Add(type, 1);
        }

    }

	public List<GameObject> collectTexts = new List<GameObject> ();

	IEnumerator ShowCollectText(ElementType type) {
		int i = 1;
		if (collectTexts.Capacity > 0) {
			foreach (GameObject text in collectTexts) {
				if (text != null) {
					i++;
					text.GetComponent<ScrollingText> ().MoveUp (i);
				}
			}
		}

		var newText = Instantiate<GameObject>(scrollingTextPrefab);
		newText.GetComponent<ScrollingText>().Setup("+1 " + type);
		newText.transform.SetParent(textCanvas.transform, false);

		collectTexts.Add (newText);

		yield return null;
	}

    public void Summoning() {
		state = HeroState.SUMMONING;
		anim.SetTrigger ("summoning");

		if (summonParticleInstance == null) {
			summonParticleInstance = GameObject.Instantiate (summonParticleSystemPrefab, transform.position, transform.rotation) as GameObject;
		}
		summonParticleInstance.GetComponent<ParticleSystem> ().Play ();
	}

	public void Normal() {
		state = HeroState.NORMAL;
		anim.SetTrigger ("normal");

		if (summonParticleInstance != null)
			summonParticleInstance.GetComponent<ParticleSystem> ().Stop ();

		foreach (Summonster monster in GameObject.FindObjectsOfType<Summonster>()) {
			monster.Follow ();
		}
	}

	public void OrderAttack() {
		state = HeroState.ORDER_ATTACK;

		foreach (Summonster monster in GameObject.FindObjectsOfType<Summonster>()) {
			monster.Attack ();
		}
	}

	public void Dead() {
		state = HeroState.DEAD;
		anim.SetTrigger ("dead");
	}

	public void InflictDamage(Enemy hitter, int damage)
	{
		var d = DamageLogic.calculateInflictedDamage(damage, ElementType.UNKNOWN, ElementType.UNKNOWN);
		health = health - d;

		var text = Instantiate<GameObject>(scrollingTextPrefab);
		text.GetComponent<ScrollingText>().Setup("-" + d);
		text.transform.SetParent(textCanvas.transform, false);

        audioSource.PlayOneShot(Utils.GetRandom(hitSounds));
	}
}
