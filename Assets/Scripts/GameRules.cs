﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public enum GameState
{
    BEGIN, PLAY, SUMMON, GAME_OVER, WIN
}

public class GameRules : MonoBehaviour {

	public GameObject startGameMenu;
    public GameObject summonMenu;
    public GameObject gameOver;
    public GameObject hud;
    public GameObject winGame;

    public AudioClip uiClick;
    private AudioSource audioSource;

    public GameState state;
    private Hero hero;
    public KeyCode restartKey;
    public KeyCode summonMenuKey;

    // Use this for initialization
    void Start () {
        hero = FindObjectOfType<Hero>();

        audioSource = gameObject.AddComponent<AudioSource>();

        Begin();
	}

    void Update () {
        if (hero.state == Hero.HeroState.DEAD)
        {
            GameOver();
        }

        if (state == GameState.BEGIN)
        {
			if (Input.GetKey (KeyCode.Return) || Input.GetKey (KeyCode.Space) || Input.GetMouseButtonDown(0)) {
				Play ();
			}
        }

        if (state == GameState.GAME_OVER)
        {
            if (Input.GetKeyDown(restartKey))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
        else if (state == GameState.PLAY)
        {
            if (Input.GetKeyDown(summonMenuKey) || Input.GetMouseButtonDown(1))
            {
                Summon();
            }
        }
        else if (state == GameState.SUMMON)
        {
            if (Input.GetKeyDown(summonMenuKey) || Input.GetMouseButtonDown(1))
            {
                Play();
            }
        }
        else if (state == GameState.WIN)
        {
            if (Input.GetKeyDown(restartKey))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
           } 
        }
	}

    public void Begin()
    {
        state = GameState.BEGIN;
		startGameMenu.SetActive (true);
        summonMenu.SetActive(false);
        hud.SetActive(false);
        gameOver.SetActive(false);
    }

    public void Play()
    {
        state = GameState.PLAY;
		startGameMenu.SetActive (false);
        summonMenu.SetActive(false);
        hud.SetActive(true);
        gameOver.SetActive(false);
    }

    public void Summon()
    {
        audioSource.PlayOneShot(uiClick);
        state = GameState.SUMMON;
		startGameMenu.SetActive (false);
        summonMenu.SetActive(true);
        hud.SetActive(false);
        gameOver.SetActive(false);
    }

    public void GameOver()
    {
        state = GameState.GAME_OVER;
		startGameMenu.SetActive (false);
        summonMenu.SetActive(false);
        hud.SetActive(false);
        gameOver.SetActive(true);
    }

    public void Win()
    {
        state = GameState.WIN;
		startGameMenu.SetActive (false);
        summonMenu.SetActive(false);
        hud.SetActive(false);
        gameOver.SetActive(false);
        winGame.SetActive(true);
    }
}
