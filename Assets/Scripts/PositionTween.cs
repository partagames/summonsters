﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class PositionTween : MonoBehaviour {

	public Vector3 targetPosOffset = new Vector3(0f, 0f ,0f);
	public float delaySeconds = 0f;
	public float duration;
	public LoopType loopType;
	public int loops;
	public Ease easeType;

	private Vector3 originalPos;
	private Tween tween;

	// Use this for initialization
	void OnEnable () {
		originalPos = transform.localPosition;
		Tween();
	}

	void Tween()
	{
		tween = transform.DOLocalMove (originalPos + targetPosOffset, duration).SetEase (easeType).SetLoops (loops, loopType);
	}

	public void KillTweens() {
		tween.Kill (false);
	}
}
