﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class DraggableUiMote : MonoBehaviour, IPointerClickHandler
{
    private AudioSource audioSource;
    private Vector3 originalPosition;

    void SetBySlotType(SlotType type, Summoner summoner, Hero hero, ElementType elementType)
    {
        switch (type)
        {
            case SlotType.BODY:
                summoner.bodyElement = elementType;
                break;
            case SlotType.HAND:
                summoner.handElement = elementType;
                break;
            case SlotType.FEET:
                summoner.feetElement = elementType;
                break;
            case SlotType.EYES:
                summoner.eyeElement = elementType;
                break;
        }
        audioSource.Play();
        hero.inventory[elementType]--;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        var type = GetComponentInParent<InventoryItem>().type;
        string[] slots = { "HandSlot", "BodySlot", "FeetSlot", "EyesSlot" };
        foreach (string tag in slots)
        {
            var slot = GameObject.FindGameObjectWithTag(tag);
            if (slot == null)
            {
                continue;
            }
            if (slot.GetComponent<Slot>().selectedType == ElementType.UNKNOWN)
            {
                SetBySlotType(slot.GetComponent<Slot>().type, FindObjectOfType<Summoner>(), FindObjectOfType<Hero>(), type);
                break;
            }
        }
    }
    
    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
