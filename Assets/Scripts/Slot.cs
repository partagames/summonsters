﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
using System;

public enum SlotType
{
    HAND, BODY, FEET, EYES
}

public class Slot : MonoBehaviour
{
    public SlotType type;
    public Sprite noSprite;
    public Sprite[] sprites;
    public Image elementImage;
    public bool selected;
    private Summoner summoner;
    public ElementType selectedType;

    void Start () {
        elementImage = GetComponentInChildren<Image>();
        summoner = FindObjectOfType<Summoner>();
	}

	void Update () {
        switch (type)
        {
            case SlotType.BODY:
                selectedType = summoner.bodyElement;
                break;
            case SlotType.HAND:
                selectedType = summoner.handElement;
                break;
            case SlotType.FEET:
                selectedType = summoner.feetElement;
                break;
            case SlotType.EYES:
                selectedType = summoner.eyeElement;
                break;
            default:
                selectedType = ElementType.UNKNOWN;
                break;
        }

        if (selectedType != ElementType.UNKNOWN)
        {
            elementImage.sprite = sprites[(int)selectedType];
        }
        else
        {
            elementImage.sprite = noSprite;
        }
    }

}
