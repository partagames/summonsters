﻿using UnityEngine;
using System.Collections;

public class DamageLogic : MonoBehaviour {

	public static float effectiveElementDamageModifier = 1.25f;
	public static float sameElementDamageModifier = 1.0f;
	public static float worseElementDamageModifier = 0.5f;

	public static int calculateInflictedDamage(int dam, ElementType attackType, ElementType defendType) {

		float damageRandomizer = Random.Range (0.7f, 1.1f);

		float damage = (float)dam * damageRandomizer;

		switch (attackType) {
		case ElementType.FIRE:
			switch (defendType) {
			case ElementType.FIRE:
				damage *= sameElementDamageModifier;
				break;
			case ElementType.ICE:
				damage *= sameElementDamageModifier;
				break;
			case ElementType.EARTH:
				damage *= worseElementDamageModifier;
				break;			
			case ElementType.AIR:
				damage *= effectiveElementDamageModifier;
				break;
			}
			break;
		case ElementType.ICE:
			switch (defendType) {
			case ElementType.FIRE:
				damage *= sameElementDamageModifier;
				break;
			case ElementType.ICE:
				damage *= sameElementDamageModifier;
				break;
			case ElementType.EARTH:
				damage *= effectiveElementDamageModifier;
				break;			
			case ElementType.AIR:
				damage *= worseElementDamageModifier;
				break;
			}
			break;
		
		case ElementType.EARTH:
			switch (defendType) {
			case ElementType.FIRE:
				damage *= effectiveElementDamageModifier;
				break;
			case ElementType.ICE:
				damage *= worseElementDamageModifier;
				break;
			case ElementType.EARTH:
				damage *= sameElementDamageModifier;
				break;			
			case ElementType.AIR:
				damage *= sameElementDamageModifier;
				break;
			}
			break;
		case ElementType.AIR:
			switch (defendType) {
			case ElementType.FIRE:
				damage *= worseElementDamageModifier;
				break;
			case ElementType.ICE:
				damage *= effectiveElementDamageModifier;
				break;
			case ElementType.EARTH:
				damage *= sameElementDamageModifier;
				break;			
			case ElementType.AIR:
				damage *= sameElementDamageModifier;
				break;
			}
			break;
		}
		return (int)damage;
	}

	public static MonsterAttributes CalculateBaseStats(MonsterAttributes[] attributes) {

		MonsterAttributes newAttributes = new MonsterAttributes ();
		int baseHealth = 0;
		int baseDamage = 0;
		float baseAttackSpeed = 0f;

		foreach (MonsterAttributes attr in attributes) {
			baseHealth += attr.baseHealth;
			baseDamage += attr.baseDamage;
			baseAttackSpeed += attr.baseAttackSpeed;
		}

		newAttributes.baseHealth = baseHealth;
		newAttributes.baseDamage = baseDamage;
		newAttributes.baseAttackSpeed = baseAttackSpeed;
		return newAttributes;
	}
}
