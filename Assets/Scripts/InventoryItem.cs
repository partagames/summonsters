﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class InventoryItem : MonoBehaviour {

    public Sprite[] inventorySprites;
    public ElementType type;

    internal void Setup(ElementType type, int count)
    {
        this.type = type;
        GetComponentInChildren<Text>().text = "" + count;
        GetComponentInChildren<Image>().sprite = inventorySprites[(int) type];
    }

    internal void SetCount(int count)
    {
        if (count <= 0)
        {
            GetComponentInChildren<Text>().enabled = false;
            GetComponentInChildren<Image>().enabled = false;
        } else
        {
            GetComponentInChildren<Text>().enabled = true;
            GetComponentInChildren<Image>().enabled = true;
            GetComponentInChildren<Text>().text = "" + count;
        }
    }

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
