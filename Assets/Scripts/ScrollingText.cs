﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

public class ScrollingText : MonoBehaviour {

	private Tween tween;

    public void Setup(string text)
    {
        this.GetComponent<Text>().text = text;
        tween = GetComponent<RectTransform>().DOLocalMoveY(100, 1).OnComplete(OnComplete);
    }

    void OnComplete()
    {
        Destroy(gameObject);
    }

	public void MoveUp(int index) {
		if (tween != null) {
			tween.Kill ();
		}
		tween = GetComponent<RectTransform>().DOLocalMoveY(100 * index, 1).OnComplete(OnComplete);
	}
}
